﻿/*
 
    Write a program that takes as input an integer N and an integer M (both N and M greater than 0). 
    You need to output the maximum number of knights that you can place in an N x M table 
    such that none of them can attack each other.

    Note: Knight is a piece of chess that can attack in the following way. K denoting the knight position, 
    X denoting the places that the knight can attack and 0 denoting the places that the knight can't attack:

        0 X 0 X 0 
        X 0 0 0 X
        0 0 K 0 0
        X 0 0 0 X
        0 X 0 X 0 

    Example:

        Case 1:
        For the input provided as follows: 8 8
        Output of the program will be:     32

        Case 2: 
        For the input provided as follows: 12 16
        Output of the program will be:     96

 */

using System;

namespace Knights
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            int n; // width
            int m; // height

            if (args.Length < 2 || (!int.TryParse(args[0], out n) || !int.TryParse(args[1], out m)) || (n <= 0 || m <= 0))
            {
                Console.WriteLine("Program requires two positive numbers as parametres, width and height of board");
                return;
            }

            // max number of knights when they are all placed on black squares
            var blackSquares = 0;

            if (n % 2 != 0)
            {
                if (m % 2 == 0)
                {
                    blackSquares += m / 2;
                }
                else
                {
                    blackSquares += (m / 2) + 1;
                }
            }

            if (n > 1)
            {
                if (n % 2 == 0)
                {
                    blackSquares += m * (n / 2);
                }
                else
                {
                    blackSquares += m * ((n - 1) / 2);
                }
            }

            var result = blackSquares;

            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
