﻿/*
   
    Write a program that takes one positive large decimal number as input. 
    Your program should output the total number of valid anagrams which can be formed using the digits of the inputted number. 
    Note: A valid anagram is a number with no zeros in the beginning of it. 

    Case 1:
      For the input provided as follows: 112
      Output of the program will be:     3
      Description of the output:
      There are exactly 3 possible numbers: 112,121, 211

    Case 2:
      For the input provided as follows: 1234
      Output of the program will be:     24

*/

using System;
using System.Linq;

namespace Anagram
{
    public static class Program
    {
        private static int Factorial(int x)
        {
            int result = x;

            for (var i = 1; i < x; i++)
            {
                result *= i;
            }

            return result;
        }

        public static void Main(string[] args)
        {
            var parameter = args.FirstOrDefault();

            if (parameter == null)
            {
                Console.WriteLine("Program requires number as a parameter");
                Console.ReadLine();
            }

            if (!decimal.TryParse(parameter, out var result))
            {
                Console.WriteLine("Provided parameter is not a valid number");
                Console.ReadLine();
            }

            var chars = parameter.Distinct()
                              .Select(c => new
                              {
                                  Char = c,
                                  Occurrences = parameter.Count(x => x == c)
                              })
                              .ToList();

            var numberOfZeros = chars.SingleOrDefault(x => x.Char == '0')?.Occurrences ?? 0;
            var numberOfPositiveDigits = parameter.Length - numberOfZeros;

            // Multinomial Coefficients formula with modification to exclude permutations starting with zero
            var permutations = numberOfPositiveDigits * Factorial(chars.Select(x => x.Occurrences).Sum() - 1);
            var uniquePermutations = permutations / chars.Select(x => Factorial(x.Occurrences)).Aggregate(1, (a, b) => a * b);

            Console.WriteLine(uniquePermutations);
            Console.ReadLine();
        }
    }
}
