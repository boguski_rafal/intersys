/*
    Write a function getDates, which will receive an unknown number of virtually every possible single argument 
    and returns a chronologically sorted array containing all valid dates (in milliseconds, starting from January 1st, 1970) 
    represented by strings matching the format 'yyyy-MM-dd'. 

    For example, if provided arguments are "2000-10-28", "1950-10-28", "2050-08-28", "1987-02-28", you should return the following array: 
    [ -605163109830, 2545322890170, 541534090170, 972756490169 ]. 

    If no valid dates (according to the specified format) are provided, your function should return NA.

*/

var moment = require('moment');


function getDates() {

    var pattern = 'yyyy-MM-dd';

    var validDates = [];

    for (var i = 0; i < arguments.length; i++) {
        var arg = arguments[i];

        if (typeof arg === 'string' || arg instanceof String) {
            var date = moment(arg, pattern.toUpperCase());

            if (date.isValid()) {
                validDates.push(date.valueOf())
            }
        }
    }

    return validDates.length > 0 ? validDates.sort()
                                 : null;
}

var result = getDates("2000-10-28", "1950-10-28", "2050-08-28", "1987-02-28")

console.log(result);
