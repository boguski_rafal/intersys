/*

   Write a function obfuscatepwd that receives a string. The string may contain digits, lower and upper case characters ([a-z] or [A-Z]), 
   or special symbols such as "#&@" (without the quotation marks). The function is required to first check whether the string is a valid password, 
   by checking if it satisfies the following conditions: 

   -	Must contain at least one digit from 0-9
   -	Must contain at least one lowercase character from a-z
   -	Must contain at least one uppercase character from A-Z
   -	Must contain at least one special symbol from #&@
   -	Must contain at least 8 characters and at most 50

   If it�s not a valid password, it should alert "invalid". If it�s a valid password, it should alert the sum of the digits 
   from the binary representation of the password.

   For example:

   obfuscatepwd("aB1@aaaaaa") alerts 27,
   obfuscatepwd("password") alerts invalid,

   and

   obfuscatepwd("1234@abcABC") alerts 31.

*/

function obfuscatepwd(password) {

    var pattern = /(?=.*\d)(?=.* [a - z])(?=.* [A - Z])(?=.* [#&@])^.{ 8, 50 } $/;

    if (!password || !pattern.test(password)) {
        alert.log('invalid');
        return
    }

    var sum = Array.from('password')
                   .reduce((acc, char) => acc.concat(char.charCodeAt().toString(2)), []) // converts each char to binary
                   .join()
                   .split('1') // compute sum of digits
                   .length - 1

    alert(sum);
};

obfuscatepwd("aB1@aaaaaa");
obfuscatepwd("password");
obfuscatepwd("1234@abcABC");
